%include 'test/test.nasm' 


section .text


test_some_failing_test:
  mov rax, -1
  ret
  

test_some_passing_test:
  mov rax, 0 
  ret
  



global _main


_main:
   
   lea  rdi, [testexample$tests]
   call test$run_tests


   jmp    exit


exit:
  mov     rax, 0x2000001 ; exit
  mov     rdi, 0
  syscall



section .data



testexample$tests:
  ISTRUC TST_LIST
    AT  TST_LIST.base_type + BASE_TYPE.type,            DD  test$tst_list_sig
    AT  TST_LIST.base_type + BASE_TYPE.instance_size,   DD  0

    AT  TST_LIST.tests + ARRAY.width,                   DD  8
    AT  TST_LIST.tests + ARRAY.capacity,                DD  2
    AT  TST_LIST.tests + ARRAY.size,                    DD  2
    AT  TST_LIST.tests + ARRAY.body,                    DQ  testexample$testpassed, testexample$testfailed

  IEND



testexample$testpassed:
   ISTRUC TST

     AT  TST.base_type + BASE_TYPE.type,            DD  test$tst_sig
     AT  TST.base_type + BASE_TYPE.instance_size,   DD  0

     AT  TST.test_fn,                               DQ  test_some_passing_test 

     AT  TST.test_name + BASE_TYPE.type,            DD  string$sig
     AT  TST.test_name + BASE_TYPE.instance_size,   DD  0
     AT  TST.test_name + ARRAY.width,               DD  8
     AT  TST.test_name + ARRAY.capacity,            DD  24
     AT  TST.test_name + ARRAY.size,                DD  24
     AT  TST.test_name + ARRAY.body,                DB  'This is the passing test'

   IEND


testexample$testfailed:
   ISTRUC TST

     AT  TST.base_type + BASE_TYPE.type,            DD  test$tst_sig
     AT  TST.base_type + BASE_TYPE.instance_size,   DD  0

     AT  TST.test_fn,                               DQ  test_some_failing_test 

     AT  TST.test_name + BASE_TYPE.type,            DD  string$sig
     AT  TST.test_name + BASE_TYPE.instance_size,   DD  0
     AT  TST.test_name + ARRAY.width,               DD  8
     AT  TST.test_name + ARRAY.capacity,            DD  24
     AT  TST.test_name + ARRAY.size,                DD  24
     AT  TST.test_name + ARRAY.body,                DB  'This is the failing test'

   IEND


debug:
  ISTRUC STRING
     AT  STRING.array + BASE_TYPE.type,            DD  string$sig
     AT  STRING.array + BASE_TYPE.instance_size,   DD  12
     AT  STRING.array + ARRAY.width,               DD  12
     AT  STRING.array + ARRAY.capacity,            DD  12
     AT  STRING.array + ARRAY.size,                DD  12
     AT  STRING.array + ARRAY.body,                DB  'DEBUG STRING'
   IEND

