%ifndef test_lib

%define test_lib

  %include 'base/base.nasm' 
  %include 'array/array.nasm' 
  %include 'string/string.nasm' 
  %include 'print/print.nasm' 


STRUC TST
  .base_type      RESB   BASE_TYPE_size
  .test_fn        RESQ   1 
  .test_name      RESB   STRING_size
ENDSTRUC



STRUC TST_LIST
   .base_type     RESB   BASE_TYPE_size
   .tests         RESQ   ARRAY_size
ENDSTRUC


test$tst_sig:       equ  3
test$tst_list_sig:  equ  4


section .text




test$runner:
  ; input  TST  {rdi}

  mov r8, rdi

  ; print test name
  add rdi, TST.test_name
  call print

  ; read and call the test function
  add r8, TST.test_fn
  mov rcx, [r8]

  call rcx

  cmp rax, -1

  je .test_failed

  ; print pass
    lea rdi, [test$passed]
    call println

    jmp .done
  
  .test_failed:
    ; print failed
    lea rdi, [test$failed]
    call println
  

  .done: 
    ret




test$run_tests:
  ; input TST_LIST   {rdi}
   
  ; check to make sure there are items to   
  mov r8, rdi
  mov ecx, [r8 + TST_LIST.tests + ARRAY.size]
  mov rdx, r8 
  add rdx, TST_LIST.tests + ARRAY.body
  
  cmp rcx, 0
  je .done

  .loop:
     
     save_regs 
     mov rdi, [rdx]
     call test$runner
     restore_regs

     add rdx, 8

     dec rcx
     jnz .loop
 

  .done: 
    ret




section .data


test$passed:
  ISTRUC STRING
     AT  STRING.array + BASE_TYPE.type,            DD  string$sig
     AT  STRING.array + BASE_TYPE.instance_size,   DD  0
     AT  STRING.array + ARRAY.width,               DD  8
     AT  STRING.array + ARRAY.capacity,            DD  9
     AT  STRING.array + ARRAY.size,                DD  9
     AT  STRING.array + ARRAY.body,                DB  ': Passed!'
   IEND


test$failed:
  ISTRUC STRING
     AT  STRING.array + BASE_TYPE.type,            DD  string$sig
     AT  STRING.array + BASE_TYPE.instance_size,   DD  11
     AT  STRING.array + ARRAY.width,               DD  11
     AT  STRING.array + ARRAY.capacity,            DD  11
     AT  STRING.array + ARRAY.size,                DD  11
     AT  STRING.array + ARRAY.body,                DB  ': FAILED :('
   IEND


%endif
