%include 'string/string.nasm' 
%include 'print/print.nasm' 
%include 'file/file.nasm' 


section .text


global _main

_main:

  ; call open to get a file descriptor
  lea rdi, [file_path_1]
  mov rsi, 0
  mov rdx, 1 
  call file$open

  ; Read from file into a string
  push rax

  mov rdi, rax
  lea rsi, [file_buffer]
  mov rdx, 100

  call file$read

  pop rdi
  push rdi

  lea rsi, [file_buffer]
  mov rdx, 100

  call file$read

  lea rdi, [file_buffer]
  call println 

  ; close file
  pop  rdi
  call file$close  ; rax should be 0 if successful

  ; open/create, write, read, print, close   
  lea rdi, [file_path_2]
  mov rsi, 1
  mov rdx, 0 

  call file$open
  push rax
 
  mov rdi, rax
  lea rsi, [file_write_buffer] 
  call file$write

  lea rdi, [file_write_buffer]
  call string$clear

  pop  rdi
  push rdi

  ; lea rsi, [file_write_buffer]
  ; mov rdx, 100
  ; call file$read

  ; print_reg rax

  
  ;lea rdi, [file_write_buffer]
  ;call println 

  pop rdi
  call file$close 

  jmp    exit


exit:
  mov     rax, 0x2000001 ; exit
  mov     rdi, 0
  syscall





section .data


file_path_1:
  ISTRUC STRING
     AT  STRING.array + BASE_TYPE.type,            DD  string$sig
     AT  STRING.array + ARRAY.width,               DD  1
     AT  STRING.array + ARRAY.capacity,            DD  100
     AT  STRING.array + ARRAY.size,                DD  7 
     AT  STRING.array + ARRAY.body,                DB  'tmp.txt'
   IEND


file_path_2:
  ISTRUC STRING
     AT  STRING.array + BASE_TYPE.type,            DD  string$sig
     AT  STRING.array + ARRAY.width,               DD  1
     AT  STRING.array + ARRAY.capacity,            DD  100
     AT  STRING.array + ARRAY.size,                DD  8 
     AT  STRING.array + ARRAY.body,                DB  'tmp2.txt'
   IEND



file_buffer:
  ISTRUC STRING
     AT  STRING.array + BASE_TYPE.type,            DD  string$sig
     AT  STRING.array + ARRAY.width,               DD  1
     AT  STRING.array + ARRAY.capacity,            DD  100
     AT  STRING.array + ARRAY.size,                DD  3 
     AT  STRING.array + ARRAY.body,                DB  ''
   IEND



file_write_buffer:
  ISTRUC STRING
     AT  STRING.array + BASE_TYPE.type,            DD  string$sig
     AT  STRING.array + ARRAY.width,               DD  1
     AT  STRING.array + ARRAY.capacity,            DD  100
     AT  STRING.array + ARRAY.size,                DD  35 
     AT  STRING.array + ARRAY.body,                DB  'hello this is the file write test!',10
   IEND

