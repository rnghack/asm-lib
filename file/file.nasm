
; BSD System calls
;3	user_ssize_t read   	int fd	user_addr_t cbuf	user_size_t nbyte
;4	user_ssize_t write	int fd	user_addr_t cbuf	user_size_t nbyte
;5	int open	user_addr_t path	int flags	int mode
;6	int close	int fd	
;60	int umask	int newmask

; File open flags
;          O_RDONLY        open for reading only
;          O_WRONLY        open for writing only
;          O_RDWR          open for reading and writing
;          O_NONBLOCK      do not block on open or for data to become available
;          O_APPEND        append on each write
;          O_CREAT         create file if it does not exist
;          O_TRUNC         truncate size to 0
;          O_EXCL          error if O_CREAT and the file exists
;          O_SHLOCK        atomically obtain a shared lock
;          O_EXLOCK        atomically obtain an exclusive lock
;          O_NOFOLLOW      do not follow symlinks
;          O_SYMLINK       allow open of symlinks


%ifndef file_lib
%define file_lib

%include 'base/base.nasm' 
%include 'array/array.nasm' 
%include 'string/string.nasm' 




section .data

file$FILE_READ_WRITE:      equ  0x0002       ; O_RDWR
file$CREATE_IF_NOT_EXIST:  equ  0x0200       ; O_CREAT
file$TRUNC_IF_EXISTS:      equ  0x400        ; O_TRUNC

file$FILE_CREATE_MASK:     equ  1204         


section .text

file$umask:
  ; input  new_mask        {rdi}
  ; output previous_mask   {rax}

   
  ; 60  int umask   int newmask
  mov rax, 60
  add rax, 0x2000000  ; OS X adjustment

  syscall

  ret


; Note will open the file for reading and writing
file$open:
  ; input  path STRING  {rdi}
  ; input  create       {rsi}
  ; input  append       {rdx}

  ; output filedescriptor int {rax}
  ; output success/error  int {rdx}

  ; set the user mask to 0, so that we can get the permissions we want
  push rdi
  push rsi
  push rdx

  mov rdi, 0
  call file$umask

  pop rdx
  pop rsi
  pop rdi

  push rsi
  push rdx

  ; make null terminated version of the path
  call string$make_null_term
  mov rdi, rax

  ; setup the flags
  pop r9
  pop r8

  mov rsi, qword file$FILE_READ_WRITE 

  ; check the create bool 
  cmp r8, 0
  je .append_flag
 
  add rsi, file$CREATE_IF_NOT_EXIST

  .append_flag:
    cmp r9, 1

  je .make_call
  
  add rsi, file$TRUNC_IF_EXISTS

  .make_call:

  mov rdx, 0q644   ; user permissions -rw-r--r--

  ; 5  int open    user_addr_t path    int flags     int mode

  ; setup the system call
  mov rax, 5
  add rax, 0x2000000  ; OS X adjustment

  syscall

  ; set return codes correctly
  mov rdx, 0

  ret



file$close:
  ; input  fd  {rdi}

  ; 6  int close  int fd	
  
  ; setup the system call
  mov rax, 6
  add rax, 0x2000000  ; OS X adjustment

  syscall

  ret



file$read:
  ; input fd             {rdi}
  ; input ARRAY          {rsi}
  ; input amount         {rdx}

  ; output bytes_read    {rax}

  push rsi

  mov r8,  rdi

  mov rdi, rsi

  call array$body_ptr
  
  mov rdi, r8
  mov rsi, rax

;3	user_ssize_t read   	int fd	user_addr_t cbuf	user_size_t nbyte


  ; setup the system call
  mov rax, 3
  add rax, 0x2000000  ; OS X adjustment

  syscall

  pop rdi

  ; set the size of the array to the amount of bytes read, IF bytes read > 0
  cmp rax, 0

  jbe .return

  mov rsi, rax
  call array$set_size


  ; return  amount read
  .return: 
    mov rdx, 0
    ret



file$write:
  ; input fd     {rdi}
  ; input ARARY  {rsi}
  
  ; 4  user_ssize_t  write  int fd  user_addr_t cbuf   user_size_t nbyte
  mov r8, rdi
  mov rdi, rsi
  call array$body_ptr


  mov rdi, r8
  xor rdx, rdx
  mov edx, [rsi + ARRAY.size]
  mov rsi, rax 


  mov rax, 4
  add rax, 0x2000000  ; OS X adjustment

  syscall

  ret

   

%endif
