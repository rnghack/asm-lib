%ifndef math_lib
%define math_lib



math$divide_long_by_long_unsigned:
  ; input   dividend  {rdi}
  ; input   divisor   {rsi} 
  ; output  result    {rax}
  ; output  remainder {rdx}

  xor   rdx, rdx

  mov   rax, rdi
  div   rsi
 
  ret



math$divide_long_by_long_signed: 
  ; input   dividend   {rdi}
  ; input   divisor    {rdx}
  ; output  result     {rax}
  ; output  remainder  {rdx} 
  
  xor   rdx, rdx

  mov   rax, rdi
  cqo
  idiv  rsi
   
  ret



%endif
