%ifndef array_lib
%define array_lib

%include 'base/base.nasm' 

section .data

array$sig:  equ  1

STRUC ARRAY 
  .type     RESB  BASE_TYPE_size
  .width    RESD  1                ; width in bytes of each item
  .capacity RESD  1
  .size     RESD  1
  .body     RESB  0
ENDSTRUC


STRUC ARRAY_128 
  .array    RESB  ARRAY_size
  .data     RESB  128 
ENDSTRUC


STRUC ARRAY_1024 
  .array    RESB  ARRAY_size
  .data     RESB  1024 
ENDSTRUC


STRUC ARRAY_4096 
  .array    RESB  ARRAY_size
  .data     RESB  4096 
ENDSTRUC

section .text


array$body_ptr:
  ; doc    return pointer to the raw buffer

  ; input  STRING {rdi}
  ; output char*  {rax}
  mov rax, rdi
  add rax, ARRAY.body

  ret


array$set_size:
  ; input STRING    {rdi}
  ; input new_size  {rsi}

  ; output new_size {rax}
  
  mov rax, rsi

  add rdi, ARRAY.size  
  mov [rdi], eax


  ret



array$append_byte:
  ; input ARRAY {rdi}
  ; input byte  {al}


  ; TODO: Check that there is capacity to add to the array before
  ;       we start
  mov r9, rdi

  xor rcx, rcx

  mov ecx, [rdi + ARRAY.size]   ; move to the item in the array
  add rdi, ARRAY.body

  add rdi, rcx

  mov [rdi], al                 ; add the item into the array 

  inc ecx                       
  mov [r9 + ARRAY.size], ecx   ; increase the size of the array

  ret


array$append_long:
  ; input ARRAY {rdi}
  ; input long  {rsi}

  ; TODO check capacity
  
  ; increase array size
  mov ecx, [rdi + ARRAY.size]
  inc ecx
  mov [rdi + ARRAY.size], ecx

  ; find memory location to add the item
  xor rdx, rdx
  xor rax, rax
  mov rax, 8
  imul ecx

  ; append item 
  add rdi, rax
  mov [rdi], rsi

  ret

   



; Returns the last item of the array and reduces its size by one
; if there are 0 items in the array will return 0
array$pop_long:
  ; input   ARRAY {rdi}
  ; output  long  {rax}

  mov ecx, [rdi + ARRAY.size]
  
  cmp ecx, 0
  jnz .pop

  ; handle empty array
  mov rax,0
  ret

  .pop:
  ; work out index of the last item
  xor edx, edx
  xor rax, rax
  mov rax, 8
  mul ecx

  ; reduce the size of the array by 1 
  dec ecx
  mov [rdi + ARRAY.size], ecx

  ; copy the last item into rax
  add rdi, rax
  mov rax, [rdi]

  


  ret



array$equal:
  ; input  ARRAY  {rdi}
  ; input  ARRAY  {rsi}
  ; output bool  success {rax}

  ; Compare item widths
  ; TODO
 
  ; Check for zero length arrarys
  ; TODO


  ; Compare length of the arrays first
  mov eax, [rdi + ARRAY.size]
  mov ecx, [rsi + ARRAY.size]

  ; Setup: we are comparing from the end
  cmp eax,ecx
  jnz .not_equals

 ; Work out how many bytes to compare
  mov eax, [rdi + ARRAY.width]
  mov edx, [rdi + ARRAY.size]

  mul edx
  mov ecx, eax
 
  add rdi, ARRAY.body  
  add rsi, ARRAY.body  
  
   .loop:
     ; load one byte at array1:rdi
     ; load one byte at array2:rsi
     mov al, [rdi]
     mov dl, [rsi] 
    
     cmp al, dl
     jnz .not_equals
 
     inc rdi
     inc rsi

     dec ecx
     jnz .loop

   mov rax, 0
   ret
   
  .not_equals:
    mov rax, -1
    ret


%endif

