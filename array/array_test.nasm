%include 'array/array.nasm' 
%include 'test/test.nasm' 


section .text

test_array_append_byte:
  lea rdi, [array_test$test_array7]
  lea rsi, [array_test$test_array8]
  
  mov al, 6

  save_regs
  call array$append_byte
  restore_regs

  lea rdi, [array_test$test_array7]
  lea rsi, [array_test$test_array8]

  call array$equal
  
  ret  
  

test_array_not_equals_length:
  lea rdi, [array_test$test_array1]
  lea rsi, [array_test$test_array2]
   
  call array$equal

  cmp rax, -1

  jne .fail
    mov rax, 0
    ret   

  .fail:
    mov rax, -1
    ret




test_array_not_equals_content:
   lea rdi, [array_test$test_array3]
   lea rsi, [array_test$test_array4]
   
   call array$equal

   cmp rax, -1

   jne .fail
     mov rax, 0
     ret   

   .fail:
     mov rax, -1
     ret




test_array_test_matches:
   lea rdi, [array_test$test_array5]
   lea rsi, [array_test$test_array6]
   
   call array$equal

   cmp rax,0 

   jne .fail
     mov rax, 0
     ret   

   .fail:
     mov rax, -1
     ret




global _main


_main:
   
   lea  rdi, [array_test$tests]
   call test$run_tests


   jmp    exit


exit:
  mov     rax, 0x2000001 ; exit
  mov     rdi, 0
  syscall






section .data



array_test$tests:
  ISTRUC TST_LIST
    AT  TST_LIST.base_type + BASE_TYPE.type,            DD  test$tst_list_sig
    AT  TST_LIST.base_type + BASE_TYPE.instance_size,   DD  0

    AT  TST_LIST.tests + ARRAY.width,                   DD  8
    AT  TST_LIST.tests + ARRAY.capacity,                DD  4
    AT  TST_LIST.tests + ARRAY.size,                    DD  4
    AT  TST_LIST.tests + ARRAY.body,                    DQ  array_test$test_array_not_equals_length,array_test$test_array_not_equals_content,array_test$test_array_test_matches,array_test$test_array_append_byte

  IEND


array_test$test_array_append_byte:
   ISTRUC TST

     AT  TST.base_type + BASE_TYPE.type,            DD  test$tst_sig
     AT  TST.base_type + BASE_TYPE.instance_size,   DD  0

     AT  TST.test_fn,                               DQ  test_array_append_byte 

     AT  TST.test_name + BASE_TYPE.type,            DD  string$sig
     AT  TST.test_name + BASE_TYPE.instance_size,   DD  0
     AT  TST.test_name + ARRAY.width,               DD  1
     AT  TST.test_name + ARRAY.capacity,            DD  31
     AT  TST.test_name + ARRAY.size,                DD  31
     AT  TST.test_name + ARRAY.body,                DB  '[Array Apppend Byte] can append'

   IEND


array_test$test_array_not_equals_length:
   ISTRUC TST

     AT  TST.base_type + BASE_TYPE.type,            DD  test$tst_sig
     AT  TST.base_type + BASE_TYPE.instance_size,   DD  0

     AT  TST.test_fn,                               DQ  test_array_not_equals_length 

     AT  TST.test_name + BASE_TYPE.type,            DD  string$sig
     AT  TST.test_name + BASE_TYPE.instance_size,   DD  0
     AT  TST.test_name + ARRAY.width,               DD  1
     AT  TST.test_name + ARRAY.capacity,            DD  43
     AT  TST.test_name + ARRAY.size,                DD  43
     AT  TST.test_name + ARRAY.body,                DB  '[Array Equals] Not equal different lengths'

   IEND




array_test$test_array_not_equals_content:
   ISTRUC TST

     AT  TST.base_type + BASE_TYPE.type,            DD  test$tst_sig
     AT  TST.base_type + BASE_TYPE.instance_size,   DD  0

     AT  TST.test_fn,                               DQ  test_array_not_equals_content

     AT  TST.test_name + BASE_TYPE.type,            DD  string$sig
     AT  TST.test_name + BASE_TYPE.instance_size,   DD  0
     AT  TST.test_name + ARRAY.width,               DD  1
     AT  TST.test_name + ARRAY.capacity,            DD  43
     AT  TST.test_name + ARRAY.size,                DD  43
     AT  TST.test_name + ARRAY.body,                DB  '[Array Equals] Not equal different content'

   IEND


array_test$test_array_test_matches:
   ISTRUC TST

     AT  TST.base_type + BASE_TYPE.type,            DD  test$tst_sig
     AT  TST.base_type + BASE_TYPE.instance_size,   DD  0

     AT  TST.test_fn,                               DQ  test_array_test_matches

     AT  TST.test_name + BASE_TYPE.type,            DD  string$sig
     AT  TST.test_name + BASE_TYPE.instance_size,   DD  0
     AT  TST.test_name + ARRAY.width,               DD  1
     AT  TST.test_name + ARRAY.capacity,            DD  34
     AT  TST.test_name + ARRAY.size,                DD  34
     AT  TST.test_name + ARRAY.body,                DB  '[Array Equals] Equal QWord content'

   IEND

 
array_test$test_array1:
   ISTRUC ARRAY
     AT  BASE_TYPE,                 DD  array$sig
     AT  BASE_TYPE.instance_size,   DD  0
     AT  ARRAY.width,               DD  1
     AT  ARRAY.capacity,            DD  3
     AT  ARRAY.size,                DD  3
     AT  ARRAY.body,                DB  '123'

   IEND


array_test$test_array2:
   ISTRUC ARRAY
     AT  BASE_TYPE,                 DD  array$sig
     AT  BASE_TYPE.instance_size,   DD  0
     AT  ARRAY.width,               DD  1
     AT  ARRAY.capacity,            DD  2
     AT  ARRAY.size,                DD  2
     AT  ARRAY.body,                DB  '23'

   IEND


array_test$test_array3:
   ISTRUC ARRAY
     AT  BASE_TYPE,                 DD  array$sig
     AT  BASE_TYPE.instance_size,   DD  0
     AT  ARRAY.width,               DD  1
     AT  ARRAY.capacity,            DD  3
     AT  ARRAY.size,                DD  3
     AT  ARRAY.body,                DB  '123'

   IEND


array_test$test_array4:
   ISTRUC ARRAY
     AT  BASE_TYPE,                 DD  array$sig
     AT  BASE_TYPE.instance_size,   DD  0
     AT  ARRAY.width,               DD  1
     AT  ARRAY.capacity,            DD  3
     AT  ARRAY.size,                DD  3
     AT  ARRAY.body,                DB  '124'

   IEND

array_test$test_array5:
   ISTRUC ARRAY
     AT  BASE_TYPE,                 DD  array$sig
     AT  BASE_TYPE.instance_size,   DD  0
     AT  ARRAY.width,               DD  8
     AT  ARRAY.capacity,            DD  8
     AT  ARRAY.size,                DD  8
     AT  ARRAY.body,                DQ  0,1,2,3,4,5,6,7

   IEND


array_test$test_array6:
   ISTRUC ARRAY
     AT  BASE_TYPE,                 DD  array$sig
     AT  BASE_TYPE.instance_size,   DD  0
     AT  ARRAY.width,               DD  8
     AT  ARRAY.capacity,            DD  8
     AT  ARRAY.size,                DD  8
     AT  ARRAY.body,                DQ  0,1,2,3,4,5,6,7

   IEND


array_test$test_array7:
   ISTRUC ARRAY
     AT  BASE_TYPE,                 DD  array$sig
     AT  BASE_TYPE.instance_size,   DD  0
     AT  ARRAY.width,               DD  1
     AT  ARRAY.capacity,            DD  100
     AT  ARRAY.size,                DD  5
     AT  ARRAY.body,                DB  1,2,3,4,5
   IEND


array_test$test_array8:
   ISTRUC ARRAY
     AT  BASE_TYPE,                 DD  array$sig
     AT  BASE_TYPE.instance_size,   DD  0
     AT  ARRAY.width,               DD  1
     AT  ARRAY.capacity,            DD  100
     AT  ARRAY.size,                DD  6
     AT  ARRAY.body,                DB  1,2,3,4,5,6
   IEND


