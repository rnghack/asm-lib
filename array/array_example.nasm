%include 'array/array.nasm' 
%include 'string/string.nasm' 
%include 'print/print.nasm' 


section .text


global _main

_main:
  ; append long
  lea rdi, [array_example$array1]
  mov rsi, 101

  call array$append_long


  ; pop long
  lea rdi, [array_example$array1]
  call array$pop_long
 
  mov rsi, rax
 
  lea rdi, [array_example$output]
  call string$append_long_signed
  
  lea rdi, [array_example$output]
  call println

  jmp    exit


exit:
  mov     rax, 0x2000001 ; exit
  mov     rdi, 0
  syscall





section .data


array_example$array1:
   ISTRUC ARRAY
     AT  BASE_TYPE,                 DD  array$sig
     AT  BASE_TYPE.instance_size,   DD  0
     AT  ARRAY.width,               DD  8
     AT  ARRAY.capacity,            DD  100
     AT  ARRAY.size,                DD  6
     AT  ARRAY.body,                DB  1,2,3,4,5,6
   IEND



array_example$output:
  ISTRUC STRING
     AT  STRING.array + BASE_TYPE.type,            DD  string$sig
     AT  STRING.array + BASE_TYPE.instance_size,   DD  0
     AT  STRING.array + ARRAY.width,               DD  1
     AT  STRING.array + ARRAY.capacity,            DD  100
     AT  STRING.array + ARRAY.size,                DD  0
     AT  STRING.array + ARRAY.body,                DB  ''
   IEND

