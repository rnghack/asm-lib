%ifndef print_lib
%define print_lib


%include 'base/base.nasm' 
%include 'array/array.nasm' 
%include 'string/string.nasm' 


section .text


print: 
   ; input STRING {rdi}
  

   ;; syscall write, rdi=fd, rsi=buff_ptr, rdx=num_of_bytes

   mov    rsi, rdi
   add    rsi, STRING.array + ARRAY.body
   
   add    rdi, STRING.array + ARRAY.size

   xor    rdx, rdx
   mov    edx, [rdi]

   mov    rdi, 1             ; std out
   mov    rax, 0x2000004     ; sys_write
 
   syscall  
     
   ret 


println:
  ; input STRING {rdi}
  
  call print

  lea rsi, [print$linefeed]
  mov rdx, 1

  mov    rdi, 1             ; std out
  mov    rax, 0x2000004     ; sys_write

  syscall

  ret



section .data
  print$linefeed:   db   10


%endif
