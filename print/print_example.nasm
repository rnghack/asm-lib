%include 'string/string.nasm' 
%include 'print/print.nasm' 



section .text


global _main

_main:
  
   lea    rdi,  [print$helloworld]
   call println

   jmp    exit


exit:
  mov     rax, 0x2000001 ; exit
  mov     rdi, 0
  syscall





section .data


print$helloworld:
  ISTRUC STRING
     AT  STRING.array + BASE_TYPE.type,            DD  string$sig
     AT  STRING.array + BASE_TYPE.instance_size,   DD  12
     AT  STRING.array + ARRAY.width,               DD  12
     AT  STRING.array + ARRAY.capacity,            DD  12
     AT  STRING.array + ARRAY.size,                DD  12
     AT  STRING.array + ARRAY.body,                DB  'Hello world!'
   IEND


