%ifndef base_lib

BITS 64
default rel


%define base_lib

%macro save_regs 0
   push  r8
   push  r9
   push  r10
   push  r11
   push  r12
   push  r13
   push  r14
   push  r15

   push  rax
   ;push  rbx
   push  rcx
   push  rdx
   push  rdi
   push  rsi
%endmacro
   

%macro restore_regs 0
   pop  rsi
   pop  rdi
   pop  rdx
   pop  rcx
   ;pop  rbx
   pop  rax

   pop  r15
   pop  r14
   pop  r13
   pop  r12
   pop  r11
   pop  r10
   pop  r9
   pop  r8
%endmacro


STRUC BASE_TYPE
  .type           RESD  1
  .instance_size  RESD  1
ENDSTRUC


%endif
