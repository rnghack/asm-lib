%ifndef string_lib
%define string_lib


%include 'base/base.nasm' 
%include 'array/array.nasm' 
%include 'math/math.nasm' 

section .data

STRUC STRING
  .array   RESB  ARRAY_128_size
ENDSTRUC


%macro string$macro_place_counter 1
  mov rax, %1
  cmp rsi, rax 
  jb .next
  inc rcx
%endmacro


%macro print_reg 1
  save_regs
  mov rsi, %1
  lea rdi, [string$debug]
  
  call string$append_long_signed

  lea rdi, [string$debug]
  call println

  lea rdi, [string$debug]
  call string$clear
  
  restore_regs
%endmacro


%macro print_bin_64 1
  save_regs
  mov rsi, %1
  lea rdi, [string$debug]
  
  call string$append_bits64

  lea rdi, [string$debug]
  call println

  lea rdi, [string$debug]
  call string$clear
  
  restore_regs
%endmacro


string$sig:  equ  2


section .text


string$init:
  ; input STRING     {rdi}
  ; input block_id   {rsi}
  ; input offset     {rdx}

  call string$init  
  mov [rdi + BASE_TYPE.type], dword string$sig

  ret


string$make_null_term:
  ; input  STRING  {rdi}

  ; output char*   {rax}
  ; output err     {rdx}

  ; TODO: Make sure string length is long enough to add null terminator

  xor rcx, rcx
  mov rax, rdi

  mov ecx, [rdi + ARRAY.size]   ; get the size of the array
  add rdi, ARRAY.body

  add rdi, rcx                  

  mov [rdi], byte 0             ; add the null terminator to the
  add rax, ARRAY.body

  ret



string$clear:
  ; input STRING {rdi}

  add rdi, ARRAY.size
  mov [rdi], dword 0

  ret



string$append_byte_as_number:
  ; input  STRING  {rdi}
  ; input  byte    {al}

  add al, 48

  call array$append_byte
   
  ret



string$append_long_unsigned:
  ; input STRING  {rdi}
  ; input value   {rsi}
  
 
  ; Find out how many digits the number has
  mov rcx, 1
  mov r9, rdi

  ; count places
  string$macro_place_counter 10
  string$macro_place_counter 100
  string$macro_place_counter 1000
  string$macro_place_counter 10000
  string$macro_place_counter 100000
  string$macro_place_counter 1000000
  string$macro_place_counter 10000000
  string$macro_place_counter 100000000
  string$macro_place_counter 1000000000
  string$macro_place_counter 10000000000
  string$macro_place_counter 100000000000
  string$macro_place_counter 1000000000000
  string$macro_place_counter 10000000000000
  string$macro_place_counter 100000000000000
  string$macro_place_counter 1000000000000000
  string$macro_place_counter 10000000000000000
  string$macro_place_counter 100000000000000000
  string$macro_place_counter 1000000000000000000   
               ; MAX  ULong  18446744073709551615
  
  .next: 
 
  ; Update the size of the string
  xor rax, rax

  mov eax, [r9 + ARRAY.size]
  add eax, ecx

  
  mov [r9 + ARRAY.size], eax

  ; Navigate to the last digit in the array
  add r9, ARRAY.body
  add r9, rax

  dec r9
 
  mov rdi, rsi
  mov rsi, 10
  

  .loop:

    call math$divide_long_by_long_unsigned 

    add rdx, 48
     
    mov [r9], dl 

    mov rdi, rax
    mov rsi, 10

    dec r9
    dec rcx

    jnz .loop
                       
  ret


string$append_long_signed:
  ; input STRING   {rdi}
  ; input value    {rsi}
  
  ; save the sign
  shl rsi, 1
  jc .handle_signed

  shr rsi, 1
  jmp string$append_long_unsigned  ; not negative so use unsigned append

  .handle_signed:

    ; append the sign to the string
    push rdi
    push rsi

    mov al, 45
    call array$append_byte 
    pop rsi
    pop rdi
 
    shr rsi, 1

    ; max value to use for subtraction
    xor rax, rax
    not rax
 
    shl rax, 1
    shr rax, 1

    ; work out actual value
    sub rax, rsi
    inc rax
    mov rsi, rax
 
    jmp string$append_long_unsigned
 



string$append_bits8:
  ; input STRING   {rdi}
  ; input value    {rsi}

  ; throw away the first 56 bits and set the loop counter to 8
  shl rsi, 56 
  mov rcx, 8

  .loop:
    shl rsi, 1

    jc .one_bit
      ; zero_bit
      mov al, 0
      jmp .done
   
      .one_bit:
        mov al, 1

    .done:
       save_regs
       call string$append_byte_as_number
       restore_regs
       
       dec rcx
       jnz .loop    
 
  ret


string$append_bits16:
  ; input STRING   {rdi}
  ; input value    {rsi}

  ; throw away the first 56 bits and set the loop counter to 8
  shl rsi, 48 
  mov rcx, 16

  .loop:
    shl rsi, 1

    jc .one_bit
      ; zero_bit
      mov al, 0
      jmp .done
   
      .one_bit:
        mov al, 1

    .done:
       save_regs
       call string$append_byte_as_number
       restore_regs
       
       dec rcx
       jnz .loop    
 
  ret



string$append_bits32:
  ; input STRING   {rdi}
  ; input value    {rsi}

  ; throw away the first 56 bits and set the loop counter to 8
  shl rsi, 32 
  mov rcx, 32

  .loop:
    shl rsi, 1

    jc .one_bit
      ; zero_bit
      mov al, 0
      jmp .done
   
      .one_bit:
        mov al, 1

    .done:
       save_regs
       call string$append_byte_as_number
       restore_regs
       
       dec rcx
       jnz .loop    
 
  ret


string$append_bits64:
  ; input STRING   {rdi}
  ; input value    {rsi}

  mov rcx, 64

  .loop:
    shl rsi, 1

    jc .one_bit
      ; zero_bit
      mov al, 0
      jmp .done
   
      .one_bit:
        mov al, 1

    .done:
       save_regs
       call string$append_byte_as_number
       restore_regs
       
       dec rcx
       jnz .loop    
 
  ret

section .data

string$debug:
  ISTRUC STRING
     AT  STRING.array + BASE_TYPE.type,            DD  string$sig
     AT  STRING.array + ARRAY.width,               DD  1
     AT  STRING.array + ARRAY.capacity,            DD  100
     AT  STRING.array + ARRAY.size,                DD  0 
     AT  STRING.array + ARRAY.body,                DB  ''
   IEND


%endif

