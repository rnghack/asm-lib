%include 'string/string.nasm' 
%include 'print/print.nasm' 


section .text


global _main

_main:
  ; println
  lea  rdi,  [print$helloworld]
  call println


  ; string$clear 
  lea  rdi,  [print$helloworld]
  call string$clear

  lea  rdi,  [print$helloworld]
  call println


  ; string$append_byte_as_number
  lea  rdi,  [print$helloworld]
  mov al, 4
  call string$append_byte_as_number

  lea  rdi,  [print$helloworld]
  call println

  lea  rdi,  [print$helloworld]
  call string$clear


  ; string$append_bits8 
  lea  rdi,  [print$helloworld]
  xor rax, rax
  mov al, 10
  mov rsi, rax
  call string$append_bits8

  lea  rdi,  [print$helloworld]
  call println

  lea  rdi,  [print$helloworld]
  call string$clear


  ; string$append_long_signed
  lea rdi, [print$helloworld]
  mov rsi, -812371 

  call string$append_long_signed

  lea  rdi,  [print$helloworld]
  call println

  jmp    exit


exit:
  mov     rax, 0x2000001 ; exit
  mov     rdi, 0
  syscall





section .data


print$helloworld:
  ISTRUC STRING
     AT  STRING.array + BASE_TYPE.type,            DD  string$sig
     AT  STRING.array + BASE_TYPE.instance_size,   DD  0
     AT  STRING.array + ARRAY.width,               DD  1
     AT  STRING.array + ARRAY.capacity,            DD  100
     AT  STRING.array + ARRAY.size,                DD  12
     AT  STRING.array + ARRAY.body,                DB  'Hello world!...........'
   IEND


